#!/bin/python3

import configparser
import psycopg2

class Settings:
    def __init__(self, file_ini):
        """
        Инициализация класса получения настроек (сразу будет создана таблица для настроек в схеме public!!!)
        str(file_ini) - Путь до локального файла настроек. В котором должны быть реквизиты для подключения к БД.
        """
        self.config = configparser.ConfigParser()
        self.config.read(file_ini)
        self.config.sections()

        try:
            self.connection = psycopg2.connect(user=self.config["database"]["user"],
                                        password=self.config["database"]["password"],
                                        host=self.config["database"]["host"],
                                        port=self.config["database"]["port"],
                                        database=self.config["database"]["dbname"],
                                        application_name = 'Settings.Init')
        except (Exception) as error:
            print("Ошибка при открытии соединения с PostgreSQL: {}".format(error))

        cursor = self.connection.cursor()
        cursor.execute("SELECT 1 FROM pg_catalog.pg_tables WHERE tablename = '_settings_';")
        if cursor.fetchone() is None:
            self.Deploy()

    def Deploy(self):
        """
        Создает таблицу для настроек
        return True
        """
        cursor = self.connection.cursor()
        cursor.execute("SET application_name = 'Settings.Deploy'")
        sql = """
        CREATE TABLE public._settings_
        (
        name text, 
        value text, 
        comment text, 
        CONSTRAINT pk_name_settings PRIMARY KEY (name)
        ) 
        WITH (
        OIDS = FALSE
        )
        ;
        """
        cursor.execute(sql)
        self.connection.commit()
        cursor.close()
        return True

    def Get(self, setting_name):
        """
        Метод получения параметра по его имени. В приоритете используется локальный файл settings.ini,
        если там нет такой настройки, то используется конфигурация в БД.
        str(setting_name) - Навание параметра

        return str or None
        """
        try:
            setting_value = self.config["main"][setting_name]
        except KeyError:
            cursor = self.connection.cursor()
            cursor.execute("SET application_name = 'Settings.Get'")
            cursor.execute("SELECT value FROM public._settings_ WHERE name = %(name)s", {"name":setting_name})
            try:
                setting_value = cursor.fetchone()[0]
            except TypeError:
                return None
        return str(setting_value)
